﻿using StudentInfo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInfo.Services
{
    public interface IStudentService
    {
        
        Task<IEnumerable<Student>> GetAllStudents();
        Task<Student> GetStudentById(int id);

        Task<Student>Create(Student student);
        Task<Student> Delete(int id);
        Task<Student> Update(Student student);
        

    }
}
