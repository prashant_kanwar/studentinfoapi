﻿using Microsoft.EntityFrameworkCore;
using StudentInfo.Data;
using StudentInfo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInfo.Services
{
    public class StudentService : IStudentService
    {
        private readonly StudentDbContext _context;

        public StudentService(StudentDbContext context)
        {
            _context = context;
        }

        public async Task<Student> Create(Student student)
        {
            var result = await _context.Students.AddAsync(student);
            await _context.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<Student> Delete(int id)
        {
            var result = await _context.Students.FirstOrDefaultAsync(s => s.Id == id);
            if(result != null)
            {
                _context.Students.Remove(result);
                await _context.SaveChangesAsync();
                return result;
            }
            return null;
        }

        public async Task<IEnumerable<Student>> GetAllStudents()
        {
            return await _context.Students.ToListAsync();
        }

        public async Task<Student> GetStudentById(int id)
        {
            return await _context.Students.FirstOrDefaultAsync(s => s.Id == id);
        }

        public async Task<Student> Update(Student student)
        {
            var result = await _context.Students.FirstOrDefaultAsync(s => s.Id == student.Id);
            if(result != null)
            {
                result.Name = student.Name;
                result.Address = student.Address;

                await _context.SaveChangesAsync();
                return result;
            }
            return null;
        }

        
    }
}
