﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StudentInfo.Models;
using StudentInfo.Services;

namespace StudentInfo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly IStudentService _service;

        public StudentsController(IStudentService service)
        {
            _service = service;
        }

        //Get api/students
        [HttpGet]
        public async Task<ActionResult> GetAllStudent()
        {
            try
            {
                return Ok(await _service.GetAllStudents());
            }
            catch(Exception)
            {
                return   StatusCode(StatusCodes.Status500InternalServerError, "" +
                    "Error retrieving data from the database");
            }

        }

        //Get api/students/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<Student>> GetStudentById(int id)
        {
            try
            {
                var student = await _service.GetStudentById(id);
                if (student == null)
                {
                    return NotFound();
                }
                return student;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "" +
                    "Error retrieving data from the database");
            }


        }

        //POST api/students
        [HttpPost]
        public async Task<ActionResult<Student>> Create(Student student)
        {
            try
            {
                if (student == null)
                {
                    return BadRequest();
                }

                var createdStudent = await _service.Create(student);
                return CreatedAtAction(nameof(GetAllStudent), new { id = createdStudent.Id }, createdStudent);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "" +
                    "Error retrieving data from the database");
            }


        }

        //DELETE api/students/{id}
        [HttpDelete("{id}")]
        public async Task<ActionResult<Student>> Delete(int id)
        {
            try
            {
                var student = await _service.GetStudentById(id);
                if (student == null)
                {
                    return NotFound($"Student with Id ={id} not found");
                }
                return await _service.Delete(id);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "" +
                    "Error retrieving data from the database");
            }

        }

        //PUT api/students/{id}
        [HttpPut("{id}")]
        public async Task<ActionResult<Student>> Update(int id, Student student)
        {
            try
            {
                if (id != student.Id)
                {
                    return BadRequest("id doesnot match");
                }
                var update = await _service.GetStudentById(id);
                if (update == null)
                {
                    return NotFound($"Student with Id ={id} not found");
                }
                return await _service.Update(student);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "" +
                    "Error retrieving data from the database");
            }



        }
    }   
        
}
